<!DOCTYPE html>
<html lang="en" dir="ltr">
    <head>
        <meta charset="utf-8">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
        <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700&display=swap" rel="stylesheet">
        <link rel="stylesheet" href="/css/font-awesome.min.css">
        <link rel="stylesheet" href="/css/main.css" />
        <title></title>
    </head>
    <body>
        <div class="container">
            <div class="row header">
                <div class="col-3 logo">
                	CODE<span>STAR</span>

                	<div class="logo-subtext">
                		школа программирования
                	</div>
                </div>

                <div class="col-5">
                	Работаем в Иркутске.<br>
                	Преподаем по всей России
                </div>

                <div class="col-2">
                	<div class="button whatsapp">
                		<div class="button-content">
	                		<i class="fa fa-whatsapp fa-2x icon" aria-hidden="true"></i>
	                		WhatsApp
	                	</div>
                	</div>
                </div>

                <div class="col-2">
                	<div class="button viber">
						<div class="button-content">
	                		<i class="fa fa-phone fa-2x icon" aria-hidden="true"></i>
	                		Viber msg
	                	</div>
                	</div>
                </div>
            </div>

            <div class="row main-content">
            	<div class="col-7">
            		<div class="main-content-header">
            			Школа профессионального программирования для детей
            		</div>
            		<div class="main-content-description">
            			пока наш сайт в разработке, ознакомиться с актуальным списком курсов и ценами можно в группе Вконтакте
            		</div>

            		<div class="button main-action-button">
            			<div class="button-content">
	            			Посетить группу Вконтакте!
	            		</div>
            		</div>
            	</div>

            	<div class="col-5">
            		<img src="/images/main.png" alt=".." width="100%">
            	</div>
            </div>

            <div class="courses-block">
            	<div class="course-block">
            		<div class="course-block-content">
	            		Основные направления по курсам CodeStar:
	            		<br><br>
	            		<a href="/">Посмотреть курсы</a>
	            	</div>
            	</div>
            	<div class="course-block">
            		<img src="/images/c1.png" />
            	</div>
            	<div class="course-block">
            		<img src="/images/c3.png" />
            	</div>
            	<div class="course-block">
            		<img src="/images/c4.png" />
            	</div>
            </div>
        </div>
    </body>
</html>
